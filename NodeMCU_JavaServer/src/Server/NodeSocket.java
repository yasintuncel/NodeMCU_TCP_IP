package Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class NodeSocket {
	
	String myName	= "-";
	String myState	= "-";
	
	Socket 				socket;
	
	PrintStream   		out;
    BufferedReader  	in;
    
    String receivedData = "";
    
	public NodeSocket(Socket s)
	{
		socket = s;
		
		try 
		{
			out = new PrintStream(s.getOutputStream());
	        in 	= new BufferedReader( new InputStreamReader	(	s.getInputStream()	 ));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
       
	}
	
	public String GetData() throws IOException
	{
		receivedData = in.readLine();
		AnalyzeReceivedData();
		return receivedData;
	}
	
	public void SendData(String data)
	{
		out.print(data);
		out.print('>');
	}

	private void AnalyzeReceivedData() 
	{
		String splittedData[];
		splittedData 	= receivedData.split("\\.");
		myName 			= splittedData[0] + "." + splittedData[1];
		myState 		= splittedData[2];
	}

}
