package Server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;

public class NMJServer {

	static ServerSocket 	serverSocket;
	static int port = 8080;
	
	public static void main(String[] args) 
	{	
		try 
		{
			serverSocket = new ServerSocket(port);
			System.out.println("Server is created. Server IP: " + InetAddress.getLocalHost().getHostAddress() + " Server Port: " + port);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		while (true)
		{
			try 
			{
            	System.out.println("Waiting for connection");
            	NodeSocket nodeSocket = new NodeSocket( serverSocket.accept() );

            	System.out.println("");
            	System.out.println(nodeSocket.socket.getInetAddress().getHostAddress() + " is connected.");
            	System.out.println("Waiting for Receive Data");
            	System.out.println("Client> " + nodeSocket.GetData() );
				System.out.println("ClientName: " + nodeSocket.myName);
				System.out.println("ClientState: " + nodeSocket.myState);
				System.out.println("");

				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				String namee = nodeSocket.myName;
				System.out.println("Waiting for connection");

				nodeSocket = new NodeSocket( serverSocket.accept() );

				System.out.println("Sending Data");
            	String data = "OK." + namee;
            	System.out.println("Sending to Client: " + data);

            	nodeSocket.SendData(data);

            	System.out.println("");
            	try {
					Thread.sleep(4000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
