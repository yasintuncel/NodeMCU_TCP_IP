nmj

this is simple server/client project on tcp/ip.

server => written with java using intellij
client => written with arduino ide. running on nodemcu

basically project is working like that.

1. clients are connecting to server when it is on.
2. clients are sending their name and state to server after first connection.

after first connection and sending data. clients need to re connect for any process about receiving or sending.

3. clients are re-connecting to server for receiving data.
4. server sending "ok.clientName" data to client which is connected.

here server and client reponses.
![result](/uploads/a3935c0d7553a5029e8e324632b221e9/result.png)


Yasin TUNCEL