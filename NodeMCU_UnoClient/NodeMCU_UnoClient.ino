#include <ESP8266WiFi.h>

char*        wifiName = "Gaziantepsporlular";     // Wifi Name
char*        wifiPass = "KelleninKefi";     // Wifi Password

int             port      = 8080;
const char *    serverIP  = "192.168.1.55"; // ip or dns

WiFiClient      client;

// data sets
String thereIs    = "K1.001.V";
String thereIsNot = "K1.001.Y";

int sensorValue = 10;   // 

void setup() 
{
  Serial.begin(115200);
  delay(10);

  ConnectToWifi();
  
}

void loop() 
{
  ConnectToServer("Connecting to Server for Send Data");
 
  String receivedData = "";
  
  if (sensorValue < 50)
  {
    Serial.print("Sending to Server> ");
    Serial.println(thereIs);
    SendDataToServer(thereIs);
  }
  else
  {
    Serial.println("Sending to Server> ");
    Serial.println(thereIsNot);
    SendDataToServer(thereIsNot);
  }

  delay(500);
  
  ConnectToServer("Connecting to server for Receive Data");  // re connect to server for receive data
  receivedData = GetDataFromServer();

  //Serial.print("Server> ");
  //Serial.println(receivedData);
  
  if (receivedData == "OK.K1.001")
  {
    Serial.print("From the Server> ");
    Serial.println(receivedData);
  }
  else if (receivedData == "DO.K1.001.WN")
  {
    // change wifi name and write it to eeprom
  }
  else if (receivedData == "DO.K1.001.WP")
  {
    // change wifi password and write it to eeprom
  }
  else if (receivedData == "DO.K1.001.SC")
  {
    // change sensor calibration and write it to eeprom
  }
  else
  {
    Serial.println("Client did not get data.");
  }
  
  //Serial.print("Server> ");
  //Serial.println(receivedData);
  CloseClient();
  
  delay(5000);
  
}

void CloseClient()
{
}

String GetDataFromServer()
{
  return client.readStringUntil('>');
}

void SendDataToServer(String data)
{
  client.print(data);
}

void ConnectToServer(String description)
{
  Serial.println("");
  Serial.println(description);
  Serial.println("Connecting to  Server.");
  Serial.print("Server IP Address: ");
  Serial.print(serverIP);
  Serial.print(":");
  Serial.println(port);
  
  if (!client.connect(serverIP, port)) 
  {
        Serial.println("connection failed");
        Serial.println("wait 5 sec...");
        delay(5000);
        return;
  }
  else
  {
    Serial.println("Connection Succes");
  }
}

void ConnectToWifi()
{
  Serial.println("");
  Serial.print("Connecting to ");
  Serial.println(wifiName);
  
  WiFi.begin(wifiName, wifiPass);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    //Serial.println("WiFi is  not connected. Please Check WifiName and Wifi Password.");
  }

  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.print("Wifi Name: ");
  Serial.println(wifiName);
  
  Serial.print("My IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println("");
}
